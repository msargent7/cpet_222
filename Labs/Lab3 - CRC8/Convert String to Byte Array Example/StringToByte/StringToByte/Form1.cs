﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StringToByte
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // declare local variables
            Encoding asciiEncoder = Encoding.ASCII;
            String inString;    // references the user input text string
            String outString;   // references recovered text from byte array
            byte[] byteAry;     // an array of bytes

            // initialize the aString variable with user supplied text string
            if (inputBox.TextLength == 0)
            {
                // error message to user
                inputBox.Text = "The application requires text";
                outputBox.Clear();
            }
            else
            {
                // ok to convert from string to byte array and back
                // initalize the aString array and clear inputBox text window
                inString = inputBox.Text;
                inputBox.Clear();

                // instantiate array 1 byte larger than string size
                byteAry = new byte[inString.Length + 1];

                // transfer string into byte array using Encoding object
                asciiEncoder.GetBytes(inString, 0, inString.Length, byteAry, 0);

                // copy a null character into last index of byte array
                byteAry[byteAry.Length - 1] = 0;

                // now copy the contents of the byte array into the outString object
                outString = asciiEncoder.GetString(byteAry, 0, byteAry.Length);

                // place the recovered text string in the outBox object
                outputBox.Text = outString;

                // prompt the user to enter another string in inputBox text window
                inputBox.Text = "Enter another string to convert";

            }   // end of else statement
        }
    }
}
