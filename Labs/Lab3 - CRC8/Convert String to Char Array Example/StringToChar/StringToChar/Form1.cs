﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StringToChar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // declare local variables
            String inString;    // references the user input text string
            String outString;   // references recovered text from byte array
            char[] charAry;     // an array of chars

            // initialize the aString variable with user supplied text string
            if (inputBox.TextLength == 0)
            {
                // error message to user
                inputBox.Text = "The application requires text";
                outputBox.Clear();
            }
            else
            {
                // ok to convert from string to byte array and back
                // initalize the aString array and clear inputBox text window
                inString = inputBox.Text;
                inputBox.Clear();

                // instantiate array 1 char larger than string size
                charAry = new char[inString.Length + 1];

                // transfer string into char array using String CopyTo method
                inString.CopyTo(0,charAry, 0, inString.Length);

                // copy a null character into last index of byte array
                charAry[charAry.Length - 1] = '\0'; // unicode null character

                // now copy the contents of the byte array into the outString object
                outString = new String( charAry );

                // place the recovered text string in the outBox object
                outputBox.Text = outString;

                // prompt the user to enter another string in inputBox text window
                inputBox.Text = "Enter another string to convert";

            }   // end of else statement
        }

        private void inputBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
