/*
 * KDG_Lab03_CRC.cpp
 *----------------------------------------
 * Simple C++ example of CRC calculation to illustrate concept. 
 *
*/

#include <iostream>
using namespace std;

const unsigned char  POLY = 0x13;   // 0001 0011 
const unsigned short MASK = 0x8000; // 1000 0000 0000 0000  

struct REG_BITS {
	unsigned crc		: 4; // LSB starts here 		
	unsigned remainder	: 1; 
	unsigned unused		: 3; 
}; 

union REGISTER {
	REG_BITS bits; 
	unsigned char byte; 
}; 

bool remainder(REGISTER reg); 

int main()
{
	REGISTER reg = { 0 }; 
	unsigned short message = 0x035B; // 0000 0011 0101 1011

	// make room for crc at end of message
	message <<= 4; // 0011 0101 1011  0000
	//                                ----
	//                                  |
	//                                 \|/
	//                                room for CRC

	unsigned short copy = message; // temporary copy of message to peel off bits

	// find CRC 
	for(int i = 0; i < 16; i++)
	{
		reg.byte <<= 1; 

		if(copy & MASK) // bit value of 1 found - shift takes care of zero append
		{
			reg.byte = reg.byte | 0x01; // append bit
		}

		copy <<= 1; // remove bit just appended to register

		if(remainder(reg)) 
		{
			reg.byte = reg.byte ^ POLY; 
		}
	}

	cout << "crc = " << hex << reg.bits.crc << endl; 

	message += reg.bits.crc; // 0x35BE

	cout << "message with CRC attached: " << hex << message << endl; 

	// now decode.  CRC should be zero

	copy = message; // temporary copy of message to peel off bits
	reg.byte = 0;   // clear register

	// find CRC 
	for(int i = 0; i < 16; i++)
	{
		reg.byte <<= 1; // shift values

		if(copy & MASK) // 1 bit found - nothing to do if zero appended
		{
			reg.byte = reg.byte | 0x01; // 0000 0001
		}

		copy <<= 1; 

		if(remainder(reg)) 
		{
			reg.byte = reg.byte ^ POLY; 
		}
	}

	cout << "result of crc check = " << hex << reg.bits.crc << endl; 


	return 0; 
}

bool remainder(REGISTER reg)
{
	REGISTER temp = { 0 }; 

	temp.bits.remainder = 1; 

	return reg.bits.remainder & temp.bits.remainder; 
}

/*
PROOF: 

crc = e
message with CRC attached: 35be
result of crc check = 0
Press any key to continue . . .

*/