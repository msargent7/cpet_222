﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ReadTextFile
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // uses a file dialog box to open the file to have the CRC calculated
        // reads the file and displays to a text box.
        // only will show .txt files in the file dialog box
        /* An OpenFileDialog object is used to display the *.txt files in the
         * working folder and allow the user to select and copy the contents
         * of the text file into the textBox2 object.
         */
        private void button1_Click(object sender, EventArgs e)
        {
            // local variables
            String info;

            // declare & instantiate an OpenFileDialog object
            OpenFileDialog fDial = new OpenFileDialog();

            // ininitalize file search to current folder
            fDial.InitialDirectory = Path.Combine(Application.StartupPath, "");

            // filter for only text files in the working folder
            fDial.Filter = "Text Files (*.txt) | *.txt";

            // show dialog, select and display text file
            if (fDial.ShowDialog() == DialogResult.OK)
            {
                // initialize String to absolute path the selected text file
                String file = fDial.FileName;
                /* instantiate and initialize a StreamReader object to the 
                 * selected text file
                 */
                StreamReader reader = new StreamReader(file);
                // copy the absolute path name of text file to textBox1
                textBox1.Text = file;
                //textBox1.AppendText(file);
                info = reader.ReadToEnd();
                textBox2.Text = (info + "\r\n\r\n");
                //textBox2.Clear();
                //textBox2.AppendText(info + "\r\n\r\n");
                reader.Close(); // close file reading resources
                //restore OpenFileDialog opening directory to original directory
                fDial.RestoreDirectory = true;
            }
            else
            {
                // something wrong if this path is taken
                textBox2.Text = "Error reading text files\r\n\r\n";
            }
        }
    }
}
