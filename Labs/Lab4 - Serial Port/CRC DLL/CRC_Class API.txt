							31 Mar 2010

// CRC_Class constructor methods
public CRC_Class(String info)
public CRC_Class(char[] chAry)
public CRC_Class(byte[] bAry)

// Computes CRC on class data and returns computed CRC to calling method
public int crcCalc()

// accessor method on private class instance data
public byte[] getbyteArray()

// converts a decimal to binary - stored in an int array
public int [] convertToBinary(int num)

// converts a decimal to octal - stored in an int array
public int [] convetToOctal (int num)


// Using the CRC_Class DLL
using CRC8_Class;  // DLL

// Copy the CRC_Class.dll file into the  ...\bin\debug folder of your project

/* Select Project\Add Reference. Now select the Browse tab in the new window.
 * Browse into the bin\debug folder where the CRC_Class.dll file was just
 * copied. Select this file and then press the OK button. The CRC_Class file
 * is now part of your .NET project and the public constructors and methods
 * listed above are now available to your application.
 */
