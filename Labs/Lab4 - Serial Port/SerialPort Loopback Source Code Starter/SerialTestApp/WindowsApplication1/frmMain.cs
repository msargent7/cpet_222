using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsApplication1
{
    public partial class frmMain : Form
    {
        
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            
        }

        private void openPortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
            {
                serialPort1.Open();
            }
            else
            {
                MessageBox.Show("Serial Port is already open.");
            }
        }

        private void closePortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
            }
            else
            {
                MessageBox.Show("The serial port must be open before it can be closed.");
            }
        }

        private void btnWrite_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Write(boxDataOut.Text);
            }
            else
            {
                MessageBox.Show("The serial port must be opened before data can be written.");
            }
        }


        private void com1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            com1ToolStripMenuItem.Checked = true;
            com2ToolStripMenuItem.Checked = false;
            if (serialPort1.IsOpen)
            {
                MessageBox.Show("The serial port must be closed before changing the COM Port");
            }
            else
            {
                serialPort1.PortName = "COM1";
            }
        }

        private void com2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            com1ToolStripMenuItem.Checked = false;
            com2ToolStripMenuItem.Checked = true;
            if (serialPort1.IsOpen)
            {
                MessageBox.Show("The serial port must be closed before changing the COM Port");
            }
            else
            {
                serialPort1.PortName = "COM2";
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            boxDataRead.Text = "";
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {

            //Not really possible for this event to be fired if it's not open, but let's test anyway.
            if (serialPort1.IsOpen)
            {
                boxDataRead.Invoke(new EventHandler(delegate
                {
                    string dataRecieved = serialPort1.ReadExisting();
                    boxDataRead.AppendText(dataRecieved);
                }));
                
            }
            else
            {
                MessageBox.Show("Serial port must be opened before reading data.");
            }
        }
    }
}