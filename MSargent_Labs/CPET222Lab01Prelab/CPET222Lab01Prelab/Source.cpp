//Michael Sargent
//Lab01 Prelab

#include <iostream>
#include <iomanip>
using namespace std;

const unsigned char bit_mask[] =
{
	0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01
	//1000 0000
	//0100 0000
	//0010 0000
	//0001 0000 etc...
};

int main()
{
	int x = 9;
	int & y = x; //& is reference
	int * p = &x; //& is an address of

	char input[32];

	char c = 'A';
	unsigned char mask = 0x80; //1000 0000

	cout << "enter a string: ";
	cin.getline(input, 32);

	int index = 0;

	cout << "ASCII" << setw(5) << "HEX" << setw(10) << "BINARY" << endl;

	while (input[index] != '\0')
	{
		cout << input[index] << setw(8) << hex << (unsigned int)input[index] << setw(6);

		for (int i = 0; i < 8; i++)
		{
			if (input[index] & bit_mask[i])
				cout << "1";
			else
				cout << "0";
			
		}

		cout << endl;
		index++;
	}

	cout << endl;

	//0000 0001
	//0000 0010

	/*//& is a bitwise and
	if (c & 0x01) //0x01 = 0000 0001
	{
		cout << "1" << endl;
	}
	else 
	{
		cout << "0" << endl;
	}*/

	cout << endl;

	return 0;
}