﻿namespace CRC_Lab
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileSelectTextBox = new System.Windows.Forms.TextBox();
            this.OpenFileButton = new System.Windows.Forms.Button();
            this.contentsTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.calculateButton = new System.Windows.Forms.Button();
            this.decimalTextBox = new System.Windows.Forms.TextBox();
            this.hexTextBox = new System.Windows.Forms.TextBox();
            this.octalTextBox = new System.Windows.Forms.TextBox();
            this.binaryTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // fileSelectTextBox
            // 
            this.fileSelectTextBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.fileSelectTextBox.Location = new System.Drawing.Point(15, 16);
            this.fileSelectTextBox.Name = "fileSelectTextBox";
            this.fileSelectTextBox.ReadOnly = true;
            this.fileSelectTextBox.Size = new System.Drawing.Size(297, 20);
            this.fileSelectTextBox.TabIndex = 0;
            // 
            // OpenFileButton
            // 
            this.OpenFileButton.Location = new System.Drawing.Point(323, 16);
            this.OpenFileButton.Name = "OpenFileButton";
            this.OpenFileButton.Size = new System.Drawing.Size(91, 20);
            this.OpenFileButton.TabIndex = 1;
            this.OpenFileButton.Text = "Open File";
            this.OpenFileButton.UseVisualStyleBackColor = true;
            this.OpenFileButton.Click += new System.EventHandler(this.OpenFileButton_Click);
            // 
            // contentsTextBox
            // 
            this.contentsTextBox.Location = new System.Drawing.Point(15, 65);
            this.contentsTextBox.Multiline = true;
            this.contentsTextBox.Name = "contentsTextBox";
            this.contentsTextBox.ReadOnly = true;
            this.contentsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.contentsTextBox.Size = new System.Drawing.Size(297, 97);
            this.contentsTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Contents of Selected Text File:";
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(323, 129);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(91, 32);
            this.calculateButton.TabIndex = 4;
            this.calculateButton.Text = "Calculate CRC";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // decimalTextBox
            // 
            this.decimalTextBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.decimalTextBox.Location = new System.Drawing.Point(488, 44);
            this.decimalTextBox.Name = "decimalTextBox";
            this.decimalTextBox.ReadOnly = true;
            this.decimalTextBox.Size = new System.Drawing.Size(112, 20);
            this.decimalTextBox.TabIndex = 5;
            // 
            // hexTextBox
            // 
            this.hexTextBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.hexTextBox.Location = new System.Drawing.Point(488, 75);
            this.hexTextBox.Name = "hexTextBox";
            this.hexTextBox.ReadOnly = true;
            this.hexTextBox.Size = new System.Drawing.Size(112, 20);
            this.hexTextBox.TabIndex = 6;
            // 
            // octalTextBox
            // 
            this.octalTextBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.octalTextBox.Location = new System.Drawing.Point(488, 108);
            this.octalTextBox.Name = "octalTextBox";
            this.octalTextBox.ReadOnly = true;
            this.octalTextBox.Size = new System.Drawing.Size(112, 20);
            this.octalTextBox.TabIndex = 7;
            // 
            // binaryTextBox
            // 
            this.binaryTextBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.binaryTextBox.Location = new System.Drawing.Point(488, 141);
            this.binaryTextBox.Name = "binaryTextBox";
            this.binaryTextBox.ReadOnly = true;
            this.binaryTextBox.Size = new System.Drawing.Size(112, 20);
            this.binaryTextBox.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(485, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "CRC RESULTS:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(434, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Decimal:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(443, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Binary:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(447, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Octal:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(453, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Hex:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 179);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.binaryTextBox);
            this.Controls.Add(this.octalTextBox);
            this.Controls.Add(this.hexTextBox);
            this.Controls.Add(this.decimalTextBox);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.contentsTextBox);
            this.Controls.Add(this.OpenFileButton);
            this.Controls.Add(this.fileSelectTextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fileSelectTextBox;
        private System.Windows.Forms.Button OpenFileButton;
        private System.Windows.Forms.TextBox contentsTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.TextBox decimalTextBox;
        private System.Windows.Forms.TextBox hexTextBox;
        private System.Windows.Forms.TextBox octalTextBox;
        private System.Windows.Forms.TextBox binaryTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

