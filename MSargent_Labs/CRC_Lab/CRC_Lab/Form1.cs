﻿/*
Michael Sargent
CPET 222
Lab02 - CRC
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CRC_Lab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void OpenFileButton_Click(object sender, EventArgs e)
        {
            //The following code launches windows explorer at the
            //C:\ directory for the user to select text files
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.Title = "Browse Text Files";

            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;

            openFileDialog1.DefaultExt = "txt";
            openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            //If the file opened, display the file path in the
            //fileSelectedTextBox and display the contents of the
            //file in the contentsTextBox
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileSelectTextBox.Text = openFileDialog1.FileName;
                contentsTextBox.Text = System.IO.File.ReadAllText(@fileSelectTextBox.Text);
            }
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            byte[] byteArray;
            byte checksum;
            int polynomial = 0x125;

            //Convert whatever is in the text box to a byte array to be sent
            //to the CRC
            byteArray = CRC.crcClass(contentsTextBox.Text);
            CRC crc = new CRC(polynomial);
            checksum = crc.Checksum(byteArray);

            //Convert the CRC to the correct formats for display
            decimalTextBox.Text = String.Format("{0:0}", checksum);
            octalTextBox.Text = System.Convert.ToString(checksum, 8);
            hexTextBox.Text = String.Format("{0:X}", checksum);
            binaryTextBox.Text = System.Convert.ToString(checksum, 2);
        }
    }

    class CRC
    {
        byte[] table = new byte[256];


        public byte Checksum(params byte[] val)
        {
            //If an empty array was sent throw an exception for the array
            if (val == null)
                throw new ArgumentNullException("val");

            byte c = 0;
            
            //XOR the bytes in the array with the current value in c
            //This will be the checksum that will be returned to the
            //main program
            foreach (byte b in val)
            {
                c = table[c ^ b];
            }

            return c;
        }

        public byte[] GenerateTable(int polynomial)
        {
            byte[] csTable = new byte[256];

            for (int i = 0; i < 256; ++i)
            {
                int curr = i;

                //Iterate through the bytes and perform an XOR on the
                //byte with the polynomial constant that is passed in
                //The polynomial can be changed to determine what you want
                //for a checksum value
                for (int j = 0; j < 8; ++j)
                {
                    //bit and curr an 0x80
                    //if the check is true then...
                    if ((curr & 0x80) != 0)
                    {
                        //...shift curr left 1 then XOR with the polynomial constant
                        curr = (curr << 1) ^ polynomial;
                    }
                    //no need for an XOR so...
                    else
                    {
                        //...shift curr left 1
                        curr <<= 1;
                    }
                }

                csTable[i] = (byte)curr;
            }

            return csTable;
        }

        public CRC(int polynomial)
        {
            this.table = this.GenerateTable(polynomial);
        }

        
        public static byte[] crcClass(String inString)
        {
            Encoding asciiEncoder = Encoding.ASCII;
            byte[] byteArray;

            //Create the array to based on the size of the string coming in
            //This will hold the bytes for the converted string
            byteArray = new byte[inString.Length];
        
            //Transfer string into byte array using encoding object
            asciiEncoder.GetBytes(inString, 0, inString.Length, byteArray, 0);

            return byteArray;
        }
        public static byte[] crcClass(char[] array)
        {
            Encoding asciiEncoder = Encoding.ASCII;
            byte[] byteArray;

            //Create the array to based on the size of the char array coming in
            //This will hold the bytes for the converted char array
            byteArray = new byte[array.Length];

            //Tranfer the char arry into a byte array using encoding object
            asciiEncoder.GetBytes(array, 0, array.Length, byteArray, 0);

            return byteArray;
        }

        public static byte[] crcClass(byte[] array)
        {
            return array;
        }
    }
}
