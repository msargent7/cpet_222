/*
Michael Sargent
CPET 222
Lab01 - Encoding
*/

#include <iostream>
#include <list>
#include <algorithm>
#include <string>

using namespace std;

const unsigned char bit_set[] = { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };

struct NODE
{
	char c;
	int freq;
	NODE * left;
	NODE * right;
};

struct ENC
{
	string c;
	int bit_count;
};

list<NODE*> nodeList(char input[80]);
bool sortNodes(NODE *first, NODE *second);
list<NODE*> buildTree(list<NODE*> list);
ENC printTree(NODE* root, string str, ENC encoding);

int main()
{
	char input[80];
	ENC encoding;
	encoding.c = "";
	encoding.bit_count = 0;

	//Get a string to encode from the user
	cout << "enter a string: ";
	cin.getline(input, 80);
	cout << endl;

	//Send the input to the nodeList function to determine the freq of each char
	//Store these results in the charList
	list<NODE*> charList = nodeList(input);

	//For each node in charList read out the name and freq
	for (NODE * node : charList)
	{
		cout << node->c << "  " << node->freq << endl;
	}

	cout << endl;

	list<NODE*>  tree = buildTree(charList);

	encoding = printTree(tree.front(), "", encoding);
	//cout << encoding.c << endl;
	//cout << encoding.bit_count << endl;

	system("pause");
	return 0;
}

list<NODE*> nodeList(char input[80])
{
	int i = 0;
	list<NODE*> nodes;

	//Check the string until you reach the end
	while (input[i] != '\0')
	{
		//First char must be new
		bool newChar = true;

		//If the node already exists, increase the frequency
		for (NODE *node : nodes)
		{
			if (input[i] == node->c)
			{
				node->freq++;
				newChar = false;
			}
		}

		//If the character does not already exist
		//Create and initialize a new node
		if (newChar)
		{
			NODE *create = new NODE;
			create->c = input[i];
			create->freq = 1;
			create->left = nullptr;
			create->right = nullptr;
			nodes.push_back(create);
		}
		i++;

		//Sort the nodes to display the least frequent node first
		nodes.sort(sortNodes);
	}

	return nodes;
}

bool sortNodes(NODE *left, NODE *right)
{
	//Compare freq of each node to the next
	return left->freq < right->freq;
}

list<NODE*> buildTree(list<NODE*> list)
{
	NODE *left = new NODE;
	NODE *right = new NODE;

	//Read out of the list while the list contains > 1 node
	while (list.size() != 1)
	{
		//Assign the left node to the front of the list then remove it
		left = list.front();
		list.pop_front();
		//Assign the right node to the front of the list then remove
		right = list.front();
		list.pop_front();

		//Create the new root
		//The frequency of the new root will be the sum of left and right nodes
		NODE *root = new NODE;
		root->c = '\0';
		root->freq = left->freq + right->freq;
		root->left = left;
		root->right = right;

		//Push the new root to the front of the list
		list.push_front(root);
		list.sort(sortNodes);
	}

	return list;
}

ENC printTree(NODE *root, string str, ENC encoding)
{
	//Check to see if the root is null
	//If it is, return, as there is nothing else to print
	if (root == nullptr)
		return encoding;

	//Look for the null terminator
	//If found, pass the node back to print, along with
	//appending either a 0 or 1 for the nodes
	if (root->c == '\0')
	{
		printTree(root->left, str + "0", encoding);
		printTree(root->right, str + "1", encoding);
		encoding.c += str;
		encoding.bit_count += sizeof(str);
	}

	//If the node does not have a null terminator,
	//it must be the end of the branch
	//Output the node contents along with the 
	//stored string for the encoding and a create a new line
	if (root->c != '\0')
	{
		cout << root->c << ": " << str << "\n";
	}	
	

	return encoding;	
}

/*PROOF

enter a string: gathering testing bat sitting radiant yesterday

h  1
b  1
y  2
d  2
r  3
s  3
e  4
n  4
g  4
a  5
5
i  5
t  8

g: 000
d: 0010
r: 0011
a: 010
: 011
i: 100
s: 1010
h: 101100
b: 101101
y: 10111
e: 1100
n: 1101
t: 111
Press any key to continue . . .

*/