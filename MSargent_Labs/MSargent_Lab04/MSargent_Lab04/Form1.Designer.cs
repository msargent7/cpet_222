﻿namespace MSargent_Lab04
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.portMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openPort = new System.Windows.Forms.ToolStripMenuItem();
            this.closePort = new System.Windows.Forms.ToolStripMenuItem();
            this.sendText = new System.Windows.Forms.TextBox();
            this.receivedText = new System.Windows.Forms.TextBox();
            this.writeButton = new System.Windows.Forms.Button();
            this.clearReceiveButton = new System.Windows.Forms.Button();
            this.hexBox = new System.Windows.Forms.TextBox();
            this.clearHexButton = new System.Windows.Forms.Button();
            this.recvCRCText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.sendCRCText = new System.Windows.Forms.TextBox();
            this.DebugGroup = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.quitButton = new System.Windows.Forms.Button();
            this.sendRecvPort = new System.IO.Ports.SerialPort(this.components);
            this.menuStrip1.SuspendLayout();
            this.DebugGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.portMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(770, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // portMenu
            // 
            this.portMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openPort,
            this.closePort});
            this.portMenu.Name = "portMenu";
            this.portMenu.Size = new System.Drawing.Size(41, 20);
            this.portMenu.Text = "Port";
            // 
            // openPort
            // 
            this.openPort.Name = "openPort";
            this.openPort.Size = new System.Drawing.Size(152, 22);
            this.openPort.Text = "Open";
            this.openPort.Click += new System.EventHandler(this.openPort_Click);
            // 
            // closePort
            // 
            this.closePort.Name = "closePort";
            this.closePort.Size = new System.Drawing.Size(152, 22);
            this.closePort.Text = "Close";
            this.closePort.Click += new System.EventHandler(this.closePort_Click);
            // 
            // sendText
            // 
            this.sendText.Location = new System.Drawing.Point(12, 28);
            this.sendText.Multiline = true;
            this.sendText.Name = "sendText";
            this.sendText.Size = new System.Drawing.Size(415, 99);
            this.sendText.TabIndex = 1;
            // 
            // receivedText
            // 
            this.receivedText.Location = new System.Drawing.Point(12, 170);
            this.receivedText.Multiline = true;
            this.receivedText.Name = "receivedText";
            this.receivedText.ReadOnly = true;
            this.receivedText.Size = new System.Drawing.Size(415, 143);
            this.receivedText.TabIndex = 2;
            // 
            // writeButton
            // 
            this.writeButton.Location = new System.Drawing.Point(326, 135);
            this.writeButton.Name = "writeButton";
            this.writeButton.Size = new System.Drawing.Size(101, 27);
            this.writeButton.TabIndex = 3;
            this.writeButton.Text = "Write";
            this.writeButton.UseVisualStyleBackColor = true;
            this.writeButton.Click += new System.EventHandler(this.writeButton_Click);
            // 
            // clearReceiveButton
            // 
            this.clearReceiveButton.Location = new System.Drawing.Point(326, 319);
            this.clearReceiveButton.Name = "clearReceiveButton";
            this.clearReceiveButton.Size = new System.Drawing.Size(101, 27);
            this.clearReceiveButton.TabIndex = 4;
            this.clearReceiveButton.Text = "Clear Receive";
            this.clearReceiveButton.UseVisualStyleBackColor = true;
            this.clearReceiveButton.Click += new System.EventHandler(this.clearReceiveButton_Click);
            // 
            // hexBox
            // 
            this.hexBox.Location = new System.Drawing.Point(16, 35);
            this.hexBox.Multiline = true;
            this.hexBox.Name = "hexBox";
            this.hexBox.Size = new System.Drawing.Size(268, 171);
            this.hexBox.TabIndex = 5;
            // 
            // clearHexButton
            // 
            this.clearHexButton.Location = new System.Drawing.Point(183, 212);
            this.clearHexButton.Name = "clearHexButton";
            this.clearHexButton.Size = new System.Drawing.Size(101, 27);
            this.clearHexButton.TabIndex = 6;
            this.clearHexButton.Text = "Clear Hex Dump";
            this.clearHexButton.UseVisualStyleBackColor = true;
            this.clearHexButton.Click += new System.EventHandler(this.clearHexButton_Click);
            // 
            // recvCRCText
            // 
            this.recvCRCText.Location = new System.Drawing.Point(211, 246);
            this.recvCRCText.Name = "recvCRCText";
            this.recvCRCText.ReadOnly = true;
            this.recvCRCText.Size = new System.Drawing.Size(71, 20);
            this.recvCRCText.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(153, 253);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Rcv CRC";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Send CRC";
            // 
            // sendCRCText
            // 
            this.sendCRCText.Location = new System.Drawing.Point(76, 246);
            this.sendCRCText.Name = "sendCRCText";
            this.sendCRCText.ReadOnly = true;
            this.sendCRCText.Size = new System.Drawing.Size(71, 20);
            this.sendCRCText.TabIndex = 12;
            // 
            // DebugGroup
            // 
            this.DebugGroup.Controls.Add(this.label3);
            this.DebugGroup.Controls.Add(this.label1);
            this.DebugGroup.Controls.Add(this.sendCRCText);
            this.DebugGroup.Controls.Add(this.label2);
            this.DebugGroup.Controls.Add(this.recvCRCText);
            this.DebugGroup.Controls.Add(this.clearHexButton);
            this.DebugGroup.Controls.Add(this.hexBox);
            this.DebugGroup.Location = new System.Drawing.Point(456, 27);
            this.DebugGroup.Name = "DebugGroup";
            this.DebugGroup.Size = new System.Drawing.Size(301, 280);
            this.DebugGroup.TabIndex = 14;
            this.DebugGroup.TabStop = false;
            this.DebugGroup.Text = "Debug";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Hex Dump";
            // 
            // quitButton
            // 
            this.quitButton.Location = new System.Drawing.Point(656, 313);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(101, 27);
            this.quitButton.TabIndex = 15;
            this.quitButton.Text = "Quit";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.quitButton_Click);
            // 
            // sendRecvPort
            // 
            this.sendRecvPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.sendRecvPort_DataReceived);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 351);
            this.Controls.Add(this.quitButton);
            this.Controls.Add(this.DebugGroup);
            this.Controls.Add(this.clearReceiveButton);
            this.Controls.Add(this.writeButton);
            this.Controls.Add(this.receivedText);
            this.Controls.Add(this.sendText);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.DebugGroup.ResumeLayout(false);
            this.DebugGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem portMenu;
        private System.Windows.Forms.ToolStripMenuItem openPort;
        private System.Windows.Forms.ToolStripMenuItem closePort;
        private System.Windows.Forms.TextBox sendText;
        private System.Windows.Forms.TextBox receivedText;
        private System.Windows.Forms.Button writeButton;
        private System.Windows.Forms.Button clearReceiveButton;
        private System.Windows.Forms.TextBox hexBox;
        private System.Windows.Forms.Button clearHexButton;
        private System.Windows.Forms.TextBox recvCRCText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox sendCRCText;
        private System.Windows.Forms.GroupBox DebugGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button quitButton;
        private System.IO.Ports.SerialPort sendRecvPort;
    }
}

