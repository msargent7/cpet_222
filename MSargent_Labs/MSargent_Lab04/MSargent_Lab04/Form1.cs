﻿/*
 Michael Sargent
 Lab 4
 3/7/18
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CRC8_Class;
using System.IO.Ports; 

namespace MSargent_Lab04
{
    //States to be used when reading the packet
    enum State
    {
        lookingForSOH, lookingForLength, readingData, doneReading
    }

    public partial class Form1 : Form
    {
        const byte SOH = 0x01;

        public Form1()
        {
            InitializeComponent(); 
        }

        private void openPort_Click(object sender, EventArgs e)
        {
            if(sendRecvPort.IsOpen)
            {
                MessageBox.Show("Serial Port is already open.");
            }

            else
            {
                Form2 newWindow = new Form2(sendRecvPort);
                newWindow.ShowDialog();
            }
        }

        private void clearReceiveButton_Click(object sender, EventArgs e)
        {
            receivedText.Text = "";
        }

        private void clearHexButton_Click(object sender, EventArgs e)
        {
            hexBox.Text = "";
        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sendRecvPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //Not really possible for this event to be fired if it's not open, but let's test anyway.
            if (sendRecvPort.IsOpen)
            {
                receivedText.Invoke(new EventHandler(delegate
                {
                    //Declare variables to be used for reading the packet
                    var state = State.readingData;
                    int totalBytes = sendRecvPort.BytesToRead;
                    byte[] receivedPacket;
                    int b = 0;
                    int len = totalBytes+3;
                    int checksum = 0;
                    byte[] crcCalcData = new byte[totalBytes];
                    int j = 0;
                    string dataReceived = "";
                    Encoding asciiEncoder = Encoding.ASCII;

                    while (state != State.doneReading)
                    {
                        switch (state)
                        {  
                            case State.lookingForSOH:
                                b = sendRecvPort.ReadByte();
                                switch (b)
                                {
                                    //If the SOH is found, change the state to looking for length
                                    case SOH:
                                        state = State.lookingForLength;
                                        break;
                                    //Keep looking for SOH
                                    default:
                                        state = State.lookingForSOH;
                                        break;
                                }
                                break;
                            case State.lookingForLength:
                                //Read in the length of the packet
                                len = sendRecvPort.ReadByte();
                                break;
                            case State.readingData:
                                //Create the new packet based on the length sent
                                //Assign SOH and length to the first two bytes
                                receivedPacket = new byte[len];
                                receivedPacket[0] = SOH;
                                receivedPacket[1] = (byte)len;

                                //Iterate through the rest of the bytes while
                                //storing them in the received packet
                                //Store only the data bytes in the crcCalcData to recalculate
                                //the crc after transmission
                                for (int i = 2; i < len -1; i++)
                                {
                                    receivedPacket[i] = (byte)sendRecvPort.ReadByte();
                                    crcCalcData[j] = receivedPacket[i];
                                    hexBox.AppendText(string.Format("{0:X}", receivedPacket[i]));
                                    hexBox.AppendText(" ");
                                    j++;
                                }

                                //Calculate the crc of the new packet
                                CRC_Class crc = new CRC_Class(crcCalcData);
                                checksum = crc.crcCalc();
                                //Add the crc in to the end of the packet
                                receivedPacket[len - 1] = (byte)checksum;
                                //Display the crc of the received packet
                                recvCRCText.Text = string.Format("{0:X}", checksum.ToString());
                                //Convert the received packet to text to be displayed
                                dataReceived = asciiEncoder.GetString(receivedPacket, 2, receivedPacket.Length - 3);
                                receivedText.Text += dataReceived;
                                state = State.doneReading;
                                break;

                            default:
                                MessageBox.Show("Oops");
                                break;
                        }
                    }
                }));

            }
            else
            {
                MessageBox.Show("Serial port must be opened before reading data.");
            }
        }

        private void closePort_Click(object sender, EventArgs e)
        {
            if (sendRecvPort.IsOpen)
            {
                sendRecvPort.Close();
            }
            else
            {
                MessageBox.Show("The serial port must be open before it can be closed.");
            }
        }

        private void writeButton_Click(object sender, EventArgs e)
        {
            CRC_Class crc = new CRC_Class(sendText.Text);

            Encoding asciiEncoder = Encoding.ASCII;
            int checksum = crc.crcCalc();
            byte[] packet = new byte[sendText.TextLength + 3];
            int len = sendText.TextLength + 3;
            packet[0] = SOH;
            packet[1] = (byte)len;

            //Transfer string into byte array using encoding object
            byte[] bytearray = asciiEncoder.GetBytes(sendText.Text);

            //Iterate through the bytearray and assign the data to the packet
            int j = 0;
            for (int i= 2; i < bytearray.Length + 2; i++)
            {
                packet[i] = bytearray[j];
                j++;
            }

            //Display the checksum of the send packet
            sendCRCText.Text = string.Format("{0:X}", checksum.ToString());

            packet[len - 1] = (byte)crc.crcCalc();

            if (sendRecvPort.IsOpen)
            {
                sendRecvPort.Write(sendText.Text);
            }
            else
            {
                MessageBox.Show("The serial port must be opened before data can be written.");
            }
        }
    }
}
