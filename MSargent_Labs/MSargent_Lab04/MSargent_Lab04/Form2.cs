﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace MSargent_Lab04
{
    public partial class Form2 : Form
    {
        SerialPort port;
        public Form2(SerialPort SP)
        {
            InitializeComponent();

            portComboBox.SelectedIndex = 0;
            dataComboBox.SelectedIndex = 0;
            baudComboBox.SelectedIndex = 3;
            flowComboBox.SelectedIndex = 0;
            parityComboBox.SelectedIndex = 2;
            stopComboBox.SelectedIndex = 0;

            port = SP;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            port.PortName = portComboBox.SelectedItem.ToString();

            switch(dataComboBox.SelectedIndex)
            {
                case 0:
                    port.DataBits = 8;
                    break;
                case 1:
                    port.DataBits = 7;
                    break;
                default: port.DataBits = 8;
                    break;
            }

            switch(baudComboBox.SelectedIndex)
            {
                case 0:
                    port.BaudRate = 1200;
                    break;
                case 1:
                    port.BaudRate = 2400;
                    break;
                case 2:
                    port.BaudRate = 4800;
                    break;
                case 3:
                    port.BaudRate = 9600;
                    break;
                case 4:
                    port.BaudRate = 14400;
                    break;
                case 5:
                    port.BaudRate = 28800;
                    break;
                case 6:
                    port.BaudRate = 33300;
                    break;
                default:
                    port.BaudRate = 9600;
                    break;
            }

            switch(flowComboBox.SelectedIndex)
            {
                case 0:
                    port.Handshake = Handshake.None; 
                    break;
                case 1:
                    port.Handshake = Handshake.XOnXOff;
                    break;
                case 2:
                    port.Handshake = Handshake.RequestToSend;
                    break;
                default:
                    port.Handshake = Handshake.None;
                    break;
            }

            switch(parityComboBox.SelectedIndex)
            {
                case 0:
                    port.Parity = Parity.Odd;
                    break;
                case 1:
                    port.Parity = Parity.Even;
                    break;
                case 2:
                    port.Parity = Parity.None;
                    break;
                default:
                    port.Parity = Parity.None;
                    break;
            }

            switch(stopComboBox.SelectedIndex)
            {
                case 0:
                    port.StopBits = StopBits.One;
                    break;
                case 1:
                    port.StopBits = StopBits.Two;
                    break;
                default:
                    break;
            }

            port.Open();
            this.Close();
        }
    }
}
