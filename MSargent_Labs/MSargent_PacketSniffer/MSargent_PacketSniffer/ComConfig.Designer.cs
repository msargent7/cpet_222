﻿namespace MSargent_PacketSniffer
{
    partial class ComConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.port1ComboBox = new System.Windows.Forms.ComboBox();
            this.port2ComboBox = new System.Windows.Forms.ComboBox();
            this.flowControlComboBox = new System.Windows.Forms.ComboBox();
            this.dataComboBox = new System.Windows.Forms.ComboBox();
            this.parityComboBox = new System.Windows.Forms.ComboBox();
            this.baudRateComboBox = new System.Windows.Forms.ComboBox();
            this.stopComboBox = new System.Windows.Forms.ComboBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.configureButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // port1ComboBox
            // 
            this.port1ComboBox.FormattingEnabled = true;
            this.port1ComboBox.Location = new System.Drawing.Point(108, 15);
            this.port1ComboBox.Name = "port1ComboBox";
            this.port1ComboBox.Size = new System.Drawing.Size(123, 21);
            this.port1ComboBox.TabIndex = 0;
            // 
            // port2ComboBox
            // 
            this.port2ComboBox.FormattingEnabled = true;
            this.port2ComboBox.Location = new System.Drawing.Point(108, 57);
            this.port2ComboBox.Name = "port2ComboBox";
            this.port2ComboBox.Size = new System.Drawing.Size(123, 21);
            this.port2ComboBox.TabIndex = 1;
            // 
            // flowControlComboBox
            // 
            this.flowControlComboBox.FormattingEnabled = true;
            this.flowControlComboBox.Items.AddRange(new object[] {
            "None",
            "XOnXOff",
            "Request To Send"});
            this.flowControlComboBox.Location = new System.Drawing.Point(108, 104);
            this.flowControlComboBox.Name = "flowControlComboBox";
            this.flowControlComboBox.Size = new System.Drawing.Size(123, 21);
            this.flowControlComboBox.TabIndex = 2;
            // 
            // dataComboBox
            // 
            this.dataComboBox.FormattingEnabled = true;
            this.dataComboBox.Items.AddRange(new object[] {
            "8 Bit",
            "7 Bit"});
            this.dataComboBox.Location = new System.Drawing.Point(108, 150);
            this.dataComboBox.Name = "dataComboBox";
            this.dataComboBox.Size = new System.Drawing.Size(123, 21);
            this.dataComboBox.TabIndex = 3;
            // 
            // parityComboBox
            // 
            this.parityComboBox.FormattingEnabled = true;
            this.parityComboBox.Items.AddRange(new object[] {
            "Odd",
            "Even",
            "None"});
            this.parityComboBox.Location = new System.Drawing.Point(108, 198);
            this.parityComboBox.Name = "parityComboBox";
            this.parityComboBox.Size = new System.Drawing.Size(123, 21);
            this.parityComboBox.TabIndex = 4;
            // 
            // baudRateComboBox
            // 
            this.baudRateComboBox.FormattingEnabled = true;
            this.baudRateComboBox.Items.AddRange(new object[] {
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "28800",
            "33300"});
            this.baudRateComboBox.Location = new System.Drawing.Point(108, 246);
            this.baudRateComboBox.Name = "baudRateComboBox";
            this.baudRateComboBox.Size = new System.Drawing.Size(123, 21);
            this.baudRateComboBox.TabIndex = 5;
            // 
            // stopComboBox
            // 
            this.stopComboBox.FormattingEnabled = true;
            this.stopComboBox.Items.AddRange(new object[] {
            "One",
            "Two"});
            this.stopComboBox.Location = new System.Drawing.Point(108, 290);
            this.stopComboBox.Name = "stopComboBox";
            this.stopComboBox.Size = new System.Drawing.Size(123, 21);
            this.stopComboBox.TabIndex = 6;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(132, 333);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(99, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // configureButton
            // 
            this.configureButton.Location = new System.Drawing.Point(13, 333);
            this.configureButton.Name = "configureButton";
            this.configureButton.Size = new System.Drawing.Size(99, 23);
            this.configureButton.TabIndex = 8;
            this.configureButton.Text = "Configure";
            this.configureButton.UseVisualStyleBackColor = true;
            this.configureButton.Click += new System.EventHandler(this.configureButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Port 1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Port 2:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Flow Control:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Data:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "Parity:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Baud Rate:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 291);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "Stop:";
            // 
            // ComConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(243, 369);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.configureButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.stopComboBox);
            this.Controls.Add(this.baudRateComboBox);
            this.Controls.Add(this.parityComboBox);
            this.Controls.Add(this.dataComboBox);
            this.Controls.Add(this.flowControlComboBox);
            this.Controls.Add(this.port2ComboBox);
            this.Controls.Add(this.port1ComboBox);
            this.Name = "ComConfig";
            this.Text = "ComConfig";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox port1ComboBox;
        private System.Windows.Forms.ComboBox port2ComboBox;
        private System.Windows.Forms.ComboBox flowControlComboBox;
        private System.Windows.Forms.ComboBox dataComboBox;
        private System.Windows.Forms.ComboBox parityComboBox;
        private System.Windows.Forms.ComboBox baudRateComboBox;
        private System.Windows.Forms.ComboBox stopComboBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button configureButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}