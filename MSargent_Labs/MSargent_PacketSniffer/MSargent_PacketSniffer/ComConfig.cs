﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace MSargent_PacketSniffer
{
    public partial class ComConfig : Form
    {
        SerialPort port1;
        SerialPort port2;
        private string[] portNames;   //stores names of equipped COM ports 
        public ComConfig(SerialPort sp1, SerialPort sp2)
        {
            port1 = sp1;
            port2 = sp2;

            InitializeComponent();
            this.Text = "Port Config";
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;

            loadComboPortBox();
            port1ComboBox.SelectedIndex = 0;
            port2ComboBox.SelectedIndex = 0;
            dataComboBox.SelectedIndex = 0;
            baudRateComboBox.SelectedIndex = 3;
            flowControlComboBox.SelectedIndex = 0;
            parityComboBox.SelectedIndex = 2;
            stopComboBox.SelectedIndex = 0;
        }

        private void loadComboPortBox()
        {
            // local variable declarations
            int length;

            /* GetPortNames() is a public static method on the SerialPort
             * class so can invoke directly from the SerialPort class. This will
             * produce an array of detected COM ports on this machine (e.g.
             * COM1, COM2, etc
             */
            this.portNames = SerialPort.GetPortNames();

            /* Now initialize the declared comboBox control in the Form with
             * the array of COM port names detected above
             */
            foreach (string name in portNames)
            {
                port1ComboBox.Items.Add(name);
                port2ComboBox.Items.Add(name);
            }

            // sort the COM port names stored in the comboBox control
            port1ComboBox.Sorted = true;
            port2ComboBox.Sorted = true;

            /* lastly, initialize the comboBox displayed text to the first
             * COM port contained in the control. If empty display an empty string.
             */
            length = portNames.Length;
            if (length > 0)
            {
                port1ComboBox.Text = portNames[length - 1];
                port2ComboBox.Text = portNames[length - 1];
            }
            else
            {
                port1ComboBox.Text = "";    // empty string
                port2ComboBox.Text = "";    // empty string
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void configureButton_Click(object sender, EventArgs e)
        {
            port1.PortName = port1ComboBox.Text;
            port2.PortName = port2ComboBox.Text;

            switch (dataComboBox.SelectedIndex)
            {
                case 0:
                    port1.DataBits = 8;
                    port2.DataBits = 8;
                    break;
                case 1:
                    port1.DataBits = 7;
                    port2.DataBits = 7;
                    break;
                default:
                    port1.DataBits = 8;
                    port2.DataBits = 8;
                    break;
            }

            switch (baudRateComboBox.SelectedIndex)
            {
                case 0:
                    port1.BaudRate = 1200;
                    port2.BaudRate = 1200;
                    break;
                case 1:
                    port1.BaudRate = 2400;
                    port2.BaudRate = 2400;
                    break;
                case 2:
                    port1.BaudRate = 4800;
                    port2.BaudRate = 4800;
                    break;
                case 3:
                    port1.BaudRate = 9600;
                    port2.BaudRate = 9600;
                    break;
                case 4:
                    port1.BaudRate = 14400;
                    port2.BaudRate = 14400;
                    break;
                case 5:
                    port1.BaudRate = 28800;
                    port2.BaudRate = 28800;
                    break;
                case 6:
                    port1.BaudRate = 33300;
                    port2.BaudRate = 33300;
                    break;
                default:
                    port1.BaudRate = 9600;
                    port2.BaudRate = 9600;
                    break;
            }

            switch (flowControlComboBox.SelectedIndex)
            {
                case 0:
                    port1.Handshake = Handshake.None;
                    port2.Handshake = Handshake.None;
                    break;
                case 1:
                    port1.Handshake = Handshake.XOnXOff;
                    port2.Handshake = Handshake.XOnXOff;
                    break;
                case 2:
                    port1.Handshake = Handshake.RequestToSend;
                    port2.Handshake = Handshake.RequestToSend;
                    break;
                default:
                    port1.Handshake = Handshake.None;
                    port2.Handshake = Handshake.None;
                    break;
            }

            switch (parityComboBox.SelectedIndex)
            {
                case 0:
                    port1.Parity = Parity.Odd;
                    port2.Parity = Parity.Odd;
                    break;
                case 1:
                    port1.Parity = Parity.Even;
                    port2.Parity = Parity.Even;
                    break;
                case 2:
                    port1.Parity = Parity.None;
                    port2.Parity = Parity.None;
                    break;
                default:
                    port1.Parity = Parity.None;
                    port2.Parity = Parity.None;
                    break;
            }

            switch (stopComboBox.SelectedIndex)
            {
                case 0:
                    port1.StopBits = StopBits.One;
                    port2.StopBits = StopBits.One;
                    break;
                case 1:
                    port1.StopBits = StopBits.Two;
                    port2.StopBits = StopBits.Two;
                    break;
                default:
                    port1.StopBits = StopBits.One;
                    port2.StopBits = StopBits.One;
                    break;
            }

            if(port1.PortName == port2.PortName)
            {
                MessageBox.Show("Port 1 and Port 2 must be different!");
            }

            else
            {
                port1.Open();
                port2.Open();
                this.Close();
            }
        }
    }
}
