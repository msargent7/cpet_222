﻿namespace MSargent_PacketSniffer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openPort1Button = new System.Windows.Forms.Button();
            this.openPort2Button = new System.Windows.Forms.Button();
            this.closePort1Button = new System.Windows.Forms.Button();
            this.closePort2Button = new System.Windows.Forms.Button();
            this.port1StatusBox = new System.Windows.Forms.TextBox();
            this.port2StatusBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.hexRadioButton = new System.Windows.Forms.RadioButton();
            this.textRadioButton = new System.Windows.Forms.RadioButton();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comPortOptionsButton = new System.Windows.Forms.Button();
            this.setPortNamesButton = new System.Windows.Forms.Button();
            this.clearDisplaysButton = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort2 = new System.IO.Ports.SerialPort(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openPort1Button
            // 
            this.openPort1Button.Location = new System.Drawing.Point(12, 12);
            this.openPort1Button.Name = "openPort1Button";
            this.openPort1Button.Size = new System.Drawing.Size(95, 27);
            this.openPort1Button.TabIndex = 0;
            this.openPort1Button.Text = "Open Port 1";
            this.openPort1Button.UseVisualStyleBackColor = true;
            // 
            // openPort2Button
            // 
            this.openPort2Button.Location = new System.Drawing.Point(12, 45);
            this.openPort2Button.Name = "openPort2Button";
            this.openPort2Button.Size = new System.Drawing.Size(95, 27);
            this.openPort2Button.TabIndex = 1;
            this.openPort2Button.Text = "Open Port 2";
            this.openPort2Button.UseVisualStyleBackColor = true;
            // 
            // closePort1Button
            // 
            this.closePort1Button.Location = new System.Drawing.Point(113, 12);
            this.closePort1Button.Name = "closePort1Button";
            this.closePort1Button.Size = new System.Drawing.Size(95, 27);
            this.closePort1Button.TabIndex = 2;
            this.closePort1Button.Text = "Close Port 1";
            this.closePort1Button.UseVisualStyleBackColor = true;
            // 
            // closePort2Button
            // 
            this.closePort2Button.Location = new System.Drawing.Point(113, 45);
            this.closePort2Button.Name = "closePort2Button";
            this.closePort2Button.Size = new System.Drawing.Size(95, 27);
            this.closePort2Button.TabIndex = 3;
            this.closePort2Button.Text = "Close Port 2";
            this.closePort2Button.UseVisualStyleBackColor = true;
            // 
            // port1StatusBox
            // 
            this.port1StatusBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.port1StatusBox.Location = new System.Drawing.Point(214, 12);
            this.port1StatusBox.Name = "port1StatusBox";
            this.port1StatusBox.ReadOnly = true;
            this.port1StatusBox.Size = new System.Drawing.Size(97, 20);
            this.port1StatusBox.TabIndex = 4;
            // 
            // port2StatusBox
            // 
            this.port2StatusBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.port2StatusBox.Location = new System.Drawing.Point(214, 45);
            this.port2StatusBox.Name = "port2StatusBox";
            this.port2StatusBox.ReadOnly = true;
            this.port2StatusBox.Size = new System.Drawing.Size(97, 20);
            this.port2StatusBox.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.hexRadioButton);
            this.groupBox1.Controls.Add(this.textRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(332, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(162, 47);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Display Output In";
            // 
            // hexRadioButton
            // 
            this.hexRadioButton.AutoSize = true;
            this.hexRadioButton.Location = new System.Drawing.Point(87, 20);
            this.hexRadioButton.Name = "hexRadioButton";
            this.hexRadioButton.Size = new System.Drawing.Size(44, 17);
            this.hexRadioButton.TabIndex = 1;
            this.hexRadioButton.TabStop = true;
            this.hexRadioButton.Text = "Hex";
            this.hexRadioButton.UseVisualStyleBackColor = true;
            // 
            // textRadioButton
            // 
            this.textRadioButton.AutoSize = true;
            this.textRadioButton.Location = new System.Drawing.Point(22, 20);
            this.textRadioButton.Name = "textRadioButton";
            this.textRadioButton.Size = new System.Drawing.Size(46, 17);
            this.textRadioButton.TabIndex = 0;
            this.textRadioButton.TabStop = true;
            this.textRadioButton.Text = "Text";
            this.textRadioButton.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(275, 103);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(219, 147);
            this.textBox2.TabIndex = 8;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 103);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(219, 147);
            this.textBox1.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Port #1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(272, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Port #2";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(519, 49);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(189, 121);
            this.textBox3.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(543, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Serial Port Configuration";
            // 
            // comPortOptionsButton
            // 
            this.comPortOptionsButton.Location = new System.Drawing.Point(519, 188);
            this.comPortOptionsButton.Name = "comPortOptionsButton";
            this.comPortOptionsButton.Size = new System.Drawing.Size(189, 28);
            this.comPortOptionsButton.TabIndex = 14;
            this.comPortOptionsButton.Text = "Com Port Options";
            this.comPortOptionsButton.UseVisualStyleBackColor = true;
            this.comPortOptionsButton.Click += new System.EventHandler(this.comPortOptionsButton_Click);
            // 
            // setPortNamesButton
            // 
            this.setPortNamesButton.Location = new System.Drawing.Point(613, 225);
            this.setPortNamesButton.Name = "setPortNamesButton";
            this.setPortNamesButton.Size = new System.Drawing.Size(95, 25);
            this.setPortNamesButton.TabIndex = 16;
            this.setPortNamesButton.Text = "Set Port Names";
            this.setPortNamesButton.UseVisualStyleBackColor = true;
            // 
            // clearDisplaysButton
            // 
            this.clearDisplaysButton.Location = new System.Drawing.Point(519, 225);
            this.clearDisplaysButton.Name = "clearDisplaysButton";
            this.clearDisplaysButton.Size = new System.Drawing.Size(88, 25);
            this.clearDisplaysButton.TabIndex = 17;
            this.clearDisplaysButton.Text = "Clear Displays";
            this.clearDisplaysButton.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 262);
            this.Controls.Add(this.clearDisplaysButton);
            this.Controls.Add(this.setPortNamesButton);
            this.Controls.Add(this.comPortOptionsButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.port2StatusBox);
            this.Controls.Add(this.port1StatusBox);
            this.Controls.Add(this.closePort2Button);
            this.Controls.Add(this.closePort1Button);
            this.Controls.Add(this.openPort2Button);
            this.Controls.Add(this.openPort1Button);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openPort1Button;
        private System.Windows.Forms.Button openPort2Button;
        private System.Windows.Forms.Button closePort1Button;
        private System.Windows.Forms.Button closePort2Button;
        private System.Windows.Forms.TextBox port1StatusBox;
        private System.Windows.Forms.TextBox port2StatusBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton textRadioButton;
        private System.Windows.Forms.RadioButton hexRadioButton;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button comPortOptionsButton;
        private System.Windows.Forms.Button setPortNamesButton;
        private System.Windows.Forms.Button clearDisplaysButton;
        private System.IO.Ports.SerialPort serialPort1;
        private System.IO.Ports.SerialPort serialPort2;
    }
}

