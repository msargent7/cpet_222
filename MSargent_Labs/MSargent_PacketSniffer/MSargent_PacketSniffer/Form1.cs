﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSargent_PacketSniffer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Text = "Serial Packet Sniffer";
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
        }

        private void comPortOptionsButton_Click(object sender, EventArgs e)
        {
            ComConfig newWindow = new ComConfig(serialPort1, serialPort2);
            newWindow.ShowDialog();
        }
    }
}
