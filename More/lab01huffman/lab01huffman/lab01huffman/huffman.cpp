
/*
Student Name: Michael Batbouta
Date: Febuary 6, 2018
Lab Assignment: Lab 01
Project Name: Huffman Encoding
Description: take string in encode each letter by turning string into just 1's and 0's and then recode it
Limitations or issues:
Credits:

*/
using namespace std;

#include <iostream>
#include<vector>
#include <algorithm>
#include <bitset>

struct NODE {
	char data;
	int  count;
	string code;
	NODE * pzero;
	NODE * pone;
};
NODE* create_node(char letter)	// create a blank node with a certain letter
{
	NODE* temp = new NODE;
	temp->data = letter;
	temp->count = 0;
	temp->pzero = nullptr;
	temp->pone = nullptr;
	temp->code = "";
	return temp;
}

bool acompare(NODE * lhs, NODE * rhs) { return lhs->count < rhs->count; }
void Traverse(NODE * node, string past);
void inorder(NODE * node);
void encode(NODE * node, char letters);
int decode(string letters, NODE * node, int index);

string encoded;
string decoded;


int main() 
{
	int number = 0;
	string code = "gathering testing bat sitting radiant yesterday";
	int num = 0;
			// create a node for each letter in the sentance
	NODE * A = create_node('a');
	NODE * G = create_node('g');
	NODE * E = create_node('e');
	NODE * T = create_node('t');
	NODE * H = create_node('h');
	NODE * R = create_node('r');
	NODE * I = create_node('i');
	NODE * N = create_node('n');
	NODE * S = create_node('s');
	NODE * B = create_node('b');
	NODE * SPACE = create_node(' ');
	NODE * Y = create_node('y');
	NODE * D = create_node('d');

	vector<NODE*>arr;
					// put all the lets into a vector
	arr.push_back(G);
	arr.push_back(A);
	arr.push_back(T);
	arr.push_back(H);
	arr.push_back(E);
	arr.push_back(R);
	arr.push_back(I);
	arr.push_back(N);
	arr.push_back(S);
	arr.push_back(B);
	arr.push_back(D);
	arr.push_back(Y);
	arr.push_back(SPACE);


	for (int num = 0; num < code.size(); num++)			//count the number of times each letter appears
	{
		if (code[num] == 'a')
		{
			A->count += 1;
		}
		else if (code[num] == 'g')
		{
			G->count += 1;
		}
		else if (code[num] == 'e')
		{
			E->count += 1;
		}
		else if (code[num] == 't')
		{
			T->count += 1;
		}
		else if (code[num] == 'h')
		{
			H->count += 1;
		}
		else if (code[num] == 'r')
		{
			R->count += 1;
		}
		else if (code[num] == 'i')
		{
			I->count += 1;
		}
		else if (code[num] == 'n')
		{
			N->count += 1;
		}
		else if (code[num] == 's')
		{
			S->count += 1;
		}
		else if (code[num] == 'b')
		{
			B->count += 1;
		}
		else if (code[num] == ' ')
		{
			SPACE->count += 1;
		}
		else if (code[num] == 'y')
		{
			Y->count += 1;
		}
		else if (code[num] == 'd')
		{
			D->count += 1;
		}
	}	   //

	sort(arr.begin(), arr.end(), acompare);		//sort vector by amount of chars

	for (int num = 0; num < arr.size(); num++)
	{
		cout << arr[num]->data << ": " << arr[num]->count << endl;
	}

	cout << endl;

	while (arr.size() > 1)
	{
		int size = arr[0]->count + arr[1]->count;
		NODE * connect = create_node(NULL);	//make node that connects the previous ones
		connect->pzero = arr[0];
		connect->pone = arr[1];
		connect->count = size;			// size of new node is the two leafs added together
		arr.erase(arr.begin());			//delete the first 2 nodes 
		arr.erase(arr.begin());
		arr.push_back(connect);		// add it to the end of the vector
		sort(arr.begin(), arr.end(), acompare);	//sort the vector and put everything in order
	}

	Traverse(arr[0], "");		//give each letter its code
	inorder(arr[0]);			// display the tree in order

	for (int count = 0; count < code.size(); count++)
	{
		encode(arr[0], code[count]);
	}

	std::bitset<167> b3(encoded);			// convert number into a bitset to be sent over wire 

	cout << b3.to_string() << endl;

	while (number < encoded.length())		// recursively loop through all the numbers till decoded 
	{
		number = decode(encoded, arr[0], number);
	}

	cout << decoded << endl;

	return 0;
}

//************* END OF MAIN *************
void Traverse(NODE * node, string past)
{
	string left = "0";
	string right = "1";
	string temp = past;
	if (node != NULL)
	{
		if (node->data != NULL)		// if there is a letter add the code
		{
			node->code = temp;
		}
		if (node->pzero != NULL)		//add a zero to the code
		{
			temp += left;
		}
		Traverse(node->pzero, temp);	// pass it previous data to the left

		if (node->data != NULL)			//
		{
			//past += right;
			node->code = past;
		}
		if (node->pone != NULL)		//adds a one to the code then moves the tree right
		{
			past += right;
		}
		Traverse(node->pone, past); // pass it previous data 
	}
}

void inorder(NODE * node)
{
	if (!node) // end the recursion if node == nullptr
		return;
	inorder(node->pzero);            // display the left subtree
	if (node->data == NULL)
	{
		cout << node->count << endl;
	}
	else
	cout << "  " << node->count << " " << node->data << " " << node->code << endl; // display the current node
	inorder(node->pone);           // display the right subtree
}

void encode(NODE * node, char letters)		// get all the encoded string
{
	if (node != NULL)
	{
		if (node->data == letters)
		{
			encoded += node->code;		// everytime there is a letter add the coding to the string
		}
		encode(node->pzero, letters);
		encode(node->pone, letters);
	}
}


int decode(string letters, NODE * node, int index)
{
	while (node->data == NULL)				// while the charecter is a NULL continue going through the tree
	{
		if (letters[index] == '0')		// if the next charecter is a 0 go left
		{
			node = node->pzero;
			index++;
		}
		else if (letters[index] == '1')		// if the next charecter is a 1 go right
		{
			node = node->pone;
			index++;
		}
	}
	decoded += node->data;		//add each letter to the string
	return index;
}


/*
Proof:
h: 1
b: 1
d: 2
y: 2
r: 3
s: 3
g: 4
e: 4
n: 4
a: 5
i: 5
: 5
t: 8

2 d 0000
4
2 y 0001
9
5 a 001
19
5 i 010
10
5   011
47
1 h 10000
2
1 b 10001
5
3 r 1001
12
3 s 1010
7
4 g 1011
28
8 t 110
16
4 e 1110
8
4 n 1111
10110011101000011101001010111110110111101110101011001011111011011100010011100111010010110110010111110110111001001000001000111111100110001111010101101110100100000010001
10110011101000011101001010111110110111101110101011001011111011011100010011100111010010110110010111110110111001001000001000111111100110001111010101101110100100000010001
gathering testing bat sitting radiant yesterday
Press any key to continue . . .



*/